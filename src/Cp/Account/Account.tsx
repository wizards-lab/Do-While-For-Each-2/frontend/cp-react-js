import React, {useState} from "react";
import axios from 'axios';
import {Button, FormGroup, H4, InputGroup, Intent} from "@blueprintjs/core";
import {ACCOUNT_EMAIL_PATH, ACCOUNT_PATH, ACCOUNT_PHONE_PATH, AppToaster, LockButton, NoMatch} from "../../globals";
import './Account.less';
import {Route, Switch} from "react-router-dom";
import {Email} from "./Email/Email";
import {Phone} from "./Phone/Phone";
import {IMenuItem} from "../../VerticalMenu/VerticalMenu";

export const accountMenuItem: IMenuItem = {
  pathname: ACCOUNT_PATH, text: 'Account', buttonProps: {icon: 'cog'},
  submenu: [
    {pathname: ACCOUNT_EMAIL_PATH, text: 'E-mail', buttonProps: {}},
    {pathname: ACCOUNT_PHONE_PATH, text: 'Phone', buttonProps: {}}
  ]
};

export const Account: React.FC = () => (
  <Switch>
    <Route path={ACCOUNT_PATH} exact>
      <ChangePassword/>
      <DeleteAccount/>
    </Route>
    <Route path={ACCOUNT_EMAIL_PATH} component={Email} exact/>
    <Route path={ACCOUNT_PHONE_PATH} component={Phone} exact/>
    <Route path="*" component={NoMatch}/>
  </Switch>
);

const ChangePassword = () => {
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showRepeatNewPassword, setShowRepeatNewPassword] = useState(false);
  const [showCurPassword, setShowCurPassword] = useState(false);
  const [newPassword, setNewPassword] = useState('');
  const [repeatNewPassword, setRepeatNewPassword] = useState('');
  const [curPassword, setCurPassword] = useState('');
  const [isLoading, setLoading] = useState(false);

  const handlerClick = () => {
    if (!newPassword.length) {
      AppToaster.show({message: 'New password is required', intent: Intent.DANGER});
      setCurPassword('');
      return;
    } else if (newPassword.length < 8) {
      AppToaster.show({message: 'New password length must be >= 8', intent: Intent.DANGER});
      setCurPassword('');
      return;
    } else if (newPassword !== repeatNewPassword) {
      AppToaster.show({message: 'New password and repeat do not match', intent: Intent.DANGER});
      setCurPassword('');
      return;
    }

    setLoading(true);
    axios
      .post(
        'http://localhost:5000/login',
        {username: 'alex', password: '123'}
      )
      .then(res => {
        AppToaster.show({message: `Server said: ${JSON.stringify(res.data)}`, intent: Intent.SUCCESS});
        setNewPassword('');
        setRepeatNewPassword('');
      })
      .catch(err => AppToaster.show({message: `${JSON.stringify(err.status)}`, intent: Intent.DANGER}))
      .finally(() => {
        setLoading(false);
        setCurPassword('');
      });
  };

  return (
    <div className={'acc-ch-password'}>
      <H4>Change password</H4>

      <FormGroup label="New password">
        <InputGroup
          type={showNewPassword ? "text" : "password"}
          rightElement={<LockButton showPassword={showNewPassword} toggleFn={setShowNewPassword} isLoading={isLoading}/>}
          value={newPassword}
          onChange={(event: any) => setNewPassword(event.target.value)}
          disabled={isLoading}
        />
      </FormGroup>

      <FormGroup label="Repeat new password">
        <InputGroup
          type={showRepeatNewPassword ? "text" : "password"}
          rightElement={<LockButton showPassword={showRepeatNewPassword} toggleFn={setShowRepeatNewPassword} isLoading={isLoading}/>}
          value={repeatNewPassword}
          onChange={(event: any) => setRepeatNewPassword(event.target.value)}
          disabled={isLoading}
        />
      </FormGroup>

      <FormGroup label="Your current password">
        <InputGroup
          type={showCurPassword ? "text" : "password"}
          rightElement={<LockButton showPassword={showCurPassword} toggleFn={setShowCurPassword} isLoading={isLoading}/>}
          value={curPassword}
          onChange={(event: any) => setCurPassword(event.target.value)}
          disabled={isLoading}
        />
      </FormGroup>

      <Button intent={Intent.PRIMARY} minimal={true} fill={true} onClick={handlerClick} loading={isLoading}>Ok</Button>
    </div>
  );
};

const DeleteAccount = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [curPassword, setCurPassword] = useState('');
  const [isLoading, setLoading] = useState(false);

  const handlerClick = () => {

  };

  return (
    <div className={'acc-del-account'}>
      <H4>Delete account</H4>

      <FormGroup label="Your current password">
        <InputGroup
          type={showPassword ? "text" : "password"}
          rightElement={<LockButton showPassword={showPassword} toggleFn={setShowPassword} isLoading={isLoading}/>}
          value={curPassword}
          onChange={(event: any) => setCurPassword(event.target.value)}
          disabled={isLoading}
        />
      </FormGroup>

      <Button intent={Intent.DANGER} minimal={true} fill={true}>Delete my account forever</Button>
    </div>
  );
};

