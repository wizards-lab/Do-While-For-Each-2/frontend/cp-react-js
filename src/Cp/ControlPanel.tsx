import {Route, Switch, useHistory, useLocation} from "react-router-dom";
import {ACCOUNT_PATH, navigateTo, NoMatch, PROFILE_PATH, ROOT_PATH, WIDTH_DRAWER_TRIGGER} from "../globals";
import React, {useState} from "react";
import {Button, IResizeEntry, Navbar, NavbarGroup, ResizeSensor} from "@blueprintjs/core";
import {VerticalMenu} from "../VerticalMenu/VerticalMenu";
import {Profile} from "./Profile/Profile";
import {Account} from "./Account/Account";
import './ControlPanel.less';

export const ControlPanel: React.FC = () => {
  const history = useHistory();
  const location = useLocation();
  const {state} = location;

  if ([ROOT_PATH, '', undefined].includes(location.pathname)) navigateTo({history, location}, PROFILE_PATH);

  const [isMenuOpen, setMenuOpen] = useState(false);
  const toggleMenuFn = () => setMenuOpen(!isMenuOpen);

  const [isDrawerMode, setDrawerMode] = useState(false);
  const handleResizeFn = (entries: IResizeEntry[]) => setDrawerMode((entries[1]?.contentRect.width || Number.MAX_SAFE_INTEGER) < WIDTH_DRAWER_TRIGGER);

  if (state && state.hasOwnProperty('hideMenu')) {
    const {hideMenu} = state;
    delete state.hideMenu;
    setMenuOpen(!hideMenu);
  }

  const navbarElem = isDrawerMode
    ? (
      <NavbarGroup>
        <Button icon={'menu'} onClick={toggleMenuFn} large={false} text={undefined}/>
      </NavbarGroup>
    ) : (
      <NavbarGroup>
        <Navbar.Heading>Control panel</Navbar.Heading>
      </NavbarGroup>
    );
  return (
    <div className={'control-panel'}>
      <div className={'navbar'}>
        <Navbar>
          {navbarElem}
        </Navbar>
      </div>
      <ResizeSensor onResize={handleResizeFn} observeParents={true}>
        <div className={'main'}>

          <VerticalMenu isOpen={isMenuOpen} toggleMenuFn={toggleMenuFn} isDrawerMode={isDrawerMode}/>

          <Switch>
            <Route path={ROOT_PATH} component={Profile} exact/>
            <Route path={PROFILE_PATH} component={Profile}/>
            <Route path={ACCOUNT_PATH} component={Account}/>
            <Route path="*" component={NoMatch}/>
          </Switch>

        </div>
      </ResizeSensor>
    </div>
  );
};
