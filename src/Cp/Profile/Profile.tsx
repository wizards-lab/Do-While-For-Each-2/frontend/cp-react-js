import React from "react";
import {H4} from "@blueprintjs/core";
import {IMenuItem} from "../../VerticalMenu/VerticalMenu";
import {NoMatch, PROFILE_PATH} from "../../globals";
import {Route, Switch} from "react-router-dom";

export const profileMenuItem: IMenuItem = {
  pathname: PROFILE_PATH, text: 'Profile', buttonProps: {icon: 'person'},
};

export const Profile: React.FC = () => {
  return (
    <Switch>

      <Route path={PROFILE_PATH} exact>
        <H4>Profile</H4>
      </Route>

      <Route path="*">
        <NoMatch/>
      </Route>

    </Switch>
  );
};

