import React, {useState} from "react";
import {AppToaster, LockButton} from "../globals";
import {Button, FormGroup, InputGroup, Intent, Tab, Tabs} from "@blueprintjs/core";
import {useLocation} from "react-router-dom";
import ReCAPTCHA from 'react-google-recaptcha';
import './Auth.less';

const LOGIN_TAB_ID = 'login';
const CREATE_ACCOUNT_TAB_ID = 'create-account';

export const Auth: React.FC = () => {
  const location = useLocation();
  const {fromLocation} = location.state || {fromLocation: {pathname: "/"}};
  const [selectedTabId, setSelectedTabId] = useState(LOGIN_TAB_ID);

  const handlerChangeTab = (id: string) => setSelectedTabId(id);

  return (
    <div className={'auth'}>
      <Tabs id={'auth-tabs'} selectedTabId={selectedTabId} onChange={handlerChangeTab}>
        <Tab id={LOGIN_TAB_ID} title={'Login'} panel={<Login/>}/>
        <Tab id={CREATE_ACCOUNT_TAB_ID} title={'Create account'} panel={<CreateAccount/>}/>
      </Tabs>
    </div>
  );
};

const Login: React.FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [isLoading, setLoading] = useState(false);

  const handlerClick = () => {
    // if (!captchaToken) {
    //   AppToaster.show({message: 'Invalid captcha', intent: Intent.DANGER});
    //   return;
    // }
    // else if (newPassword !== repeatNewPassword) {
    //   AppToaster.show({message: 'New password and repeat do not match', intent: Intent.DANGER});
    //   return;
    // }

    setLoading(true);
    // axios
    //   .post(
    //     'http://localhost:5000/login',
    //     {username: 'alex', password: '123'}
    //   )
    //   .then(res => AppToaster.show({message: `Server said: ${JSON.stringify(res.data)}`, intent: Intent.SUCCESS}))
    //   .catch(err => AppToaster.show({message: `${JSON.stringify(err.status)}`, intent: Intent.DANGER}))
    //   .finally(() => setLoading(false));
  };

  return (
    <>
      <FormGroup label="Username">
        <InputGroup
          value={username}
          onChange={(event: any) => setUsername(event.target.value)}
          disabled={isLoading}
        />
      </FormGroup>
      <FormGroup label="Password">
        <InputGroup
          type={showPassword ? "text" : "password"}
          rightElement={<LockButton showPassword={showPassword} toggleFn={setShowPassword} isLoading={isLoading}/>}
          value={password}
          onChange={(event: any) => setPassword(event.target.value)}
          disabled={isLoading}
        />
      </FormGroup>
      <Button intent={Intent.PRIMARY} loading={isLoading} fill={true} onClick={handlerClick}>Ok</Button>
    </>
  );
};

const CreateAccount: React.FC = () => {
  const [captchaToken, setCaptchaToken] = useState('');
  const [username, setUsername] = useState('');
  const [isLoading, setLoading] = useState(false);

  const handlerCaptchaChange = (token) => setCaptchaToken(token === null ? '' : token);
  const handlerCaptchaError = () => {
    setCaptchaToken('');
    AppToaster.show({message: 'Captcha error', intent: Intent.DANGER});
  };
  const handlerClick = () => {
    if (!captchaToken) {
      AppToaster.show({message: 'Invalid captcha', intent: Intent.DANGER});
      return;
    }
    // else if (newPassword !== repeatNewPassword) {
    //   AppToaster.show({message: 'New password and repeat do not match', intent: Intent.DANGER});
    //   return;
    // }

    setLoading(true);
    // axios
    //   .post(
    //     'http://localhost:5000/login',
    //     {username: 'alex', password: '123'}
    //   )
    //   .then(res => AppToaster.show({message: `Server said: ${JSON.stringify(res.data)}`, intent: Intent.SUCCESS}))
    //   .catch(err => AppToaster.show({message: `${JSON.stringify(err.status)}`, intent: Intent.DANGER}))
    //   .finally(() => setLoading(false));
  };

  return (
    <>
      <FormGroup label="E-mail">
        <InputGroup
          value={username}
          onChange={(event: any) => setUsername(event.target.value)}
          disabled={isLoading}
        />
      </FormGroup>
      <FormGroup>
        <ReCAPTCHA
          sitekey="6LfGxk4UAAAAAJ9YrW5GuwXosCn1hawNn8YXNaTi"
          onChange={handlerCaptchaChange}
          onErrored={handlerCaptchaError}
        />
      </FormGroup>
      <Button intent={Intent.PRIMARY} fill={true} loading={isLoading} onClick={handlerClick}>Ok</Button>
    </>
  );
};
