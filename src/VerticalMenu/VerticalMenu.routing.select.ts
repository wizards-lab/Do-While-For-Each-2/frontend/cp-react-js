import {IMenuItem, ISelectData, ISubmenuItem} from "./VerticalMenu";
import {accountMenuItem} from "../Cp/Account/Account";
import {profileMenuItem} from "../Cp/Profile/Profile";

export const verticalMenuItems: IMenuItem[] = [
  profileMenuItem,
  accountMenuItem,
];
const selectDataMap = new Map<string, ISelectData>();
const fillSelectDataMap = () => {
  if (selectDataMap.size) {
    return selectDataMap;
  }
  verticalMenuItems.forEach(({pathname, submenu}) => {
    selectDataMap.set(pathname, {menuId: pathname, btnId: pathname});
    if (!!submenu) {
      submenu.forEach(({pathname: submenuPath}) => selectDataMap.set(submenuPath, {menuId: pathname, btnId: submenuPath}));
    }
  });
};
fillSelectDataMap();

export const getSelectionData = (location_pathname: string, pathname: string, submenu?: ISubmenuItem[]): ISelectData | undefined => {
  if (location_pathname === pathname) {
    return selectDataMap.get(pathname);
  } else if (!!submenu) {
    for (let i = 0; i < submenu.length; i++) {
      const {pathname} = submenu[i];
      if (location_pathname === pathname) {
        return selectDataMap.get(pathname);
      }
    }
  }
};
