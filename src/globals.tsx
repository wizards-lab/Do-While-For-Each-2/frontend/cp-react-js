import {Button, Intent, NonIdealState, Position, Toaster, Tooltip} from "@blueprintjs/core";
import React from "react";
import {useLocation} from "react-router-dom";

export const WIDTH_VERTICAL_MENU = 150;
export const WIDTH_TABLET = 768;
export const WIDTH_DRAWER_TRIGGER = WIDTH_TABLET - WIDTH_VERTICAL_MENU;

//region Routing

export const ROOT_PATH = '/';
export const LOGIN_PATH = '/login';
export const PROFILE_PATH = '/profile';
export const ACCOUNT_PATH = '/account';
export const ACCOUNT_EMAIL_PATH = `${ACCOUNT_PATH}/email`;
export const ACCOUNT_PHONE_PATH = `${ACCOUNT_PATH}/phone`;

export const navigateTo = ({history, location}, path: string, state?: INavigationState): void =>
  history.push(path, {
    ...(location.state || {}), // cтарый state
    ...state,                  // новый state
  })
;

export interface INavigationState {
  hideMenu?: boolean;
  fromLocation?: any;
}

//endregion

export const AppToaster = Toaster.create({
  position: Position.TOP,
  maxToasts: 1
});

export const LockButton = ({showPassword, toggleFn, isLoading}) => (
  <Tooltip content={`${showPassword ? 'Hide' : 'Show'}`} intent={Intent.NONE}>
    <Button
      icon={showPassword ? 'unlock' : 'lock'}
      intent={Intent.NONE}
      minimal={true}
      onClick={() => toggleFn(!showPassword)}
      disabled={isLoading}
    />
  </Tooltip>
);

export const NoMatch: React.FC = () => {
  let location = useLocation();
  return (
    <div>
      <NonIdealState
        icon={'error'}
        title={'No match'}
        description={<> for <code>{location.pathname}</code> </>}
      />
    </div>
  );
};
